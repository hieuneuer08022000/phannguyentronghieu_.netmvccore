using System;

namespace WebApplication2.Models
{
    public class ErrorViewModel
    {
        /*Test update*/
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
